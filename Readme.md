###N-Way Set Associative Cache

1. The cache itself is entirely in memory (i.e. it does not communicate with a backing store).
2. Provides LRU and MRU eviction policy.
3. Allows client to have custom eviction policy.

How to use:

Use CacheBuilder to build the cache instance.

For example, 
```
Cache<String, Integer> cache = CacheBuilder.newBuilder().
                                cacheSize(2).blockSize(3).
                                recordStats().build();
```
                                
It will create a cache of size 2 with each index having a set/block of size 3.

###Get 
```
Integer value = cache.get("hello");
```
There is getAndUpdate() method as well which is explained in @Updater section.

###Put

```
// Throws compilation error as type of value declared is Integer.
cache.put("hello", "world")

It dooesn't accept null arguments, so both key and value are non null.
```

###Delete 

Deletes by key

```
cache.delete("hello")
```

###Eviction Policies

Default eviction policy is LRU. Provided implementation for LRU and MRU.
To specify eviction policy use .

```
Cache<String, String> cache = CacheBuilder.newBuilder().
                                cacheSize(2).blockSize(3).
                                evictionPolicy(new MRUEvictionPolicy()).
                                recordStats().build();
```


###Custom Eviction Policy

CustomEvictionPolicy should implement EvictionPolicy interface.

Check LRUEvictionPolicy and MRUEvictionPolicy implementation.

###Statistics 

Use ```recordStats()``` to enable recording stats for cache.

```cache.getCacheStats()``` provides cache snapshot.

###Updater

This is useful when you want to have custom eviction policy like 
Least Frequently Used. In this case you need to maintain the frequency inside
value object. In that case, doing get and put can be very expensive. 
So you can specify the updater implementation in the method getAndUpdate(). 

Example -

```
    class TestObj {
        String value;
        Integer frequency;
    }
```
```
    Updater<TestObj> updater = new Updater<TestObj>() {
        @Override
        public TestObj update(TestObj currentValue) {
            TestObj newValue = new TestObj();
            newValue.frequency = currentValue.frequency+1;
            newValue.value = currentValue.value;
            return newValue;
        }

        @Override
        public boolean compare(TestObj oldValue, TestObj currentValue) {
            return oldValue.value.equals(currentValue.value);
        }
    };
    cache.put("hello123", new TestObj("world", 1));
    cache.getAndUpdate("hello123", updater);

```

Updater is thread safe, you don't need to worry about concurrency.

TODO:

1. Currently the implementation of CacheBlock is based on List. CacheBlockArrayListImp.
   Check for LinkedHashmap.

2. Can we add time based eviction ? Support for TTL.

3. Support for Off Heap memory.