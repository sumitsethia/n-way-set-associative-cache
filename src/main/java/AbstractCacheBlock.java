
// TODO currently the implementation is based on List. CacheBlockArrayListImp.
//  Check for LinkedHashmap. I think it can offer better lookup performance.

abstract class AbstractCacheBlock<K, V> implements CacheBlock<K, V> {

    private final StatsCounter statsCounter;
    private final Object lock = new Object();

    AbstractCacheBlock(StatsCounter statsCounter) {
        this.statsCounter = statsCounter;
    }

    @Override
    public V get(K key) {
        statsCounter.recordGet(1);
        CacheEntry<K, V> cacheEntry = doGet(key);
        if (cacheEntry != null) {
            V value = cacheEntry.getValue();
            K cacheKey = cacheEntry.getKey();
            if (cacheKey == key) {
                cacheEntry.setLastAccessTime(TimeUtils.now());
                statsCounter.recordHits(1);
                return value;
            }
        }
        statsCounter.recordMisses(1);
        return null;
    }

    @Override
    public V getAndUpdate(K key, Updater<V> updater) {
        statsCounter.recordGet(1);
        CacheEntry<K, V> cacheEntry = doGet(key);
        if (cacheEntry != null) {
            V value = cacheEntry.getValue();
            K cacheKey = cacheEntry.getKey();
            if (cacheKey == key) {
                // Tricky, the last access time doesn't need lock. And we are modifying the cache entry.
                cacheEntry.setLastAccessTime(TimeUtils.now());
                update(cacheEntry, cacheKey, value, updater);
                statsCounter.recordHits(1);
                return value;
            }
        }
        statsCounter.recordMisses(1);
        return null;
    }

    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    private void update(CacheEntry<K, V> cacheEntry, K oldKey, V oldValue, Updater<V> updater) {
        synchronized (cacheEntry) {
            if (cacheEntry.getKey() == oldKey && updater.compare(oldValue, cacheEntry.getValue())) {
                V newValue = updater.update(cacheEntry.getValue());
                doUpdate(cacheEntry, oldKey, newValue);
            }
        }
    }

    // This function can't be executed serially.
    // Also delete and put both uses the same lock.
    @Override
    public void put(K key, V value) {
        synchronized (lock) {
            statsCounter.recordPut(1);
            CacheEntry<K, V> cacheEntry = doGet(key);
            if (cacheEntry != null) {
                doUpdate(cacheEntry, key, value);
            } else {
                boolean res = doPut(key, value);
                if (!res) {
                    statsCounter.recordEviction(1);
                    evict();
                    doPut(key, value);
                }
            }
        }
    }

    @Override
    public boolean delete(K key) {
        synchronized (lock) {
            CacheEntry<K, V> cacheEntry = doGet(key);
            if (cacheEntry != null) {
                doDelete(cacheEntry);
                return true;
            }
        }
        return false;
    }

    @Override
    public CacheStats getCacheStats() {
        return statsCounter.snapshot();
    }

    abstract void evict();

    abstract CacheEntry<K, V> doGet(K key);

    abstract boolean doPut(K key, V value);

    abstract void doDelete(CacheEntry<K, V> cacheEntry);

    abstract void doUpdate(CacheEntry<K, V> cacheEntry, K key, V newValue);
}
