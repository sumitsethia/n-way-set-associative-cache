public interface Cache<K, V> {
    V get(K key);

    //TODO make updater part of cachebuilder and remove the param from this function.
    V getAndUpdate(K key, Updater<V> updater);

    void put(K key, V value);

    boolean delete(K key);

    CacheStats getCacheStats();

    Integer size();
}
