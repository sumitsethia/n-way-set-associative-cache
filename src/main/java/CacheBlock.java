public interface CacheBlock<K, V> extends Iterable<CacheEntry<K, V>> {
    V get(K key);

    V getAndUpdate(K key, Updater<V> updater);

    void put(K key, V value) throws Exception;

    boolean delete(K key);

    CacheStats getCacheStats();

    int size();
}
