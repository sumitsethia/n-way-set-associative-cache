import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CacheBlockArrayListImp<K, V> extends AbstractCacheBlock<K, V> {

    private List<CacheEntry<K, V>> cacheEntries;
    private int blockSize;
    private EvictionPolicy evictionPolicy;
    private CacheEntry<K, V> nextFreeSlot;
    private int totalAllocated;

    CacheBlockArrayListImp(int blockSize, @Nonnull EvictionPolicy evictionPolicy, @Nonnull StatsCounter statsCounter) {
        super(statsCounter);
        this.blockSize = blockSize;
        this.evictionPolicy = evictionPolicy;
        cacheEntries = new ArrayList<>();
        for (int i = 0; i < blockSize; i++) {
            cacheEntries.add(new CacheEntry<>());
        }
        nextFreeSlot = cacheEntries.get(0);
        totalAllocated = 0;
    }

    @Override
    void evict() {
        CacheEntry<K, V> cacheEntry = evictionPolicy.policy(iterator());
        assert cacheEntry != null;
        doDelete(cacheEntry);
        nextFreeSlot = cacheEntry;
    }

    @Override
    boolean doPut(K key, V value) {
        if (totalAllocated == blockSize) {
            return false;
        }
        CacheEntry<K, V> freeSlot;
        if (nextFreeSlot != null) {
            freeSlot = nextFreeSlot;
        } else {
            freeSlot = getFreeSlot();
        }
        assert freeSlot != null;
        doUpdate(freeSlot, key, value);
        nextFreeSlot = null;
        totalAllocated++;
        return true;
    }

    CacheEntry<K, V> doGet(K key) {
        for (CacheEntry<K, V> data : this) {
            if (data.getKey() == key) {
                return data;
            }
        }
        return null;
    }

    @Override
    void doDelete(CacheEntry<K, V> cacheEntry) {
        cacheEntry.setEmpty(true);
        totalAllocated--;
    }

    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    @Override
    void doUpdate(CacheEntry<K, V> cacheEntry, K key, V newValue) {
        synchronized (cacheEntry) {
            cacheEntry.set(key, newValue);
        }
    }

    private CacheEntry<K, V> getFreeSlot() {
        //TODO Can't use class's iterator as it hides empty elements. Not a good way.
        for (int i = 0; i < blockSize; i++) {
            CacheEntry<K, V> cacheEntry = cacheEntries.get(i);
            if (cacheEntry.getKey() == null || cacheEntry.isEmpty()) {
                return cacheEntry;
            }
        }
        return null;
    }

    @Override
    @Nonnull
    public Iterator<CacheEntry<K, V>> iterator() {
        return new BlockArrayListIterator();
    }

    public int size() {
        return totalAllocated;
    }

    public class BlockArrayListIterator implements Iterator<CacheEntry<K, V>> {
        int index = 0;

        BlockArrayListIterator() {
        }

        @Override
        public boolean hasNext() {
            if (index == blockSize) {
                return false;
            }
            CacheEntry<K, V> cacheEntry = cacheEntries.get(index);
            while (cacheEntry.getKey() == null || cacheEntry.isEmpty()) {
                index++;
                if (index == blockSize) {
                    break;
                }
                cacheEntry = cacheEntries.get(index);
            }
            return index != blockSize;
        }

        @Override
        public CacheEntry<K, V> next() {
            if (hasNext()) {
                CacheEntry<K, V> cacheEntry = cacheEntries.get(index);
                index++;
                return cacheEntry;
            }
            return null;
        }
    }
}
