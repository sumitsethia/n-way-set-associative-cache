import lombok.Getter;

import java.util.function.Supplier;

public final class CacheBuilder<K, V> {

    private final EvictionPolicy DEFAULT_EVICTION_POLICY = new LRUEvictionPolicy();
    @Getter
    private Integer cacheSize;
    @Getter
    private Integer blockSize;
    @Getter
    private EvictionPolicy evictionPolicy = DEFAULT_EVICTION_POLICY;

    private Supplier<StatsCounter> statsCounterSupplier = NullStatsCounter::new;
    private Supplier<StatsCounter> CACHE_STATS_COUNTER = StatsCounterImp::new;

    private CacheBuilder() {
    }

    static CacheBuilder<Object, Object> newBuilder() {
        return new CacheBuilder<>();
    }

    CacheBuilder<K, V> cacheSize(Integer cacheSize) {
        this.cacheSize = cacheSize;
        return this;
    }

    CacheBuilder<K, V> blockSize(Integer blockSize) {
        this.blockSize = blockSize;
        return this;
    }

    public CacheBuilder<K, V> evictionPolicy(EvictionPolicy evictionPolicy) {
        this.evictionPolicy = evictionPolicy;
        return this;
    }

    <K1 extends K, V1 extends V> Cache<K1, V1> build() throws Exception {
        checkArguments();
        return new NWaySetCacheStore<>(this);
    }

    CacheBuilder<K, V> recordStats() {
        statsCounterSupplier = CACHE_STATS_COUNTER;
        return this;
    }

    Supplier<StatsCounter> getStatsCounterSupplier() {
        return statsCounterSupplier;
    }

    private void checkArguments() throws Exception {
        if (this.cacheSize == null || this.cacheSize == 0) {
            throw new Exception("Cache Size can't be nil or 0. Please set cache size");
        }
        if (this.blockSize == null || this.blockSize == 0) {
            throw new Exception("Block Size can't be nil or 0. Please set block size");
        }
    }
}
