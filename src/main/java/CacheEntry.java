import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicLong;

@Getter
@ToString
class CacheEntry<K, V> {
    private volatile K key;
    private volatile boolean isEmpty;
    private volatile AtomicLong lastAccessTime = new AtomicLong();
    private volatile V value;

    CacheEntry() {
        key = null;
        isEmpty = true;
        value = null;
    }

    void set(K key, V value) {
        this.setKey(key);
        this.setValue(value);
    }

    long getLastAccessTime() {
        return this.lastAccessTime.get();
    }

    void setLastAccessTime(long time) {
        lastAccessTime.updateAndGet(value -> Math.max(value, time));
    }

    private void setValue(V value) {
        this.value = value;
        this.setLastAccessTime(TimeUtils.now());
        this.setEmpty(false);
    }

    private void setKey(K key) {
        this.key = key;
    }

    void setEmpty(boolean empty) {
        this.isEmpty = empty;
    }
}
