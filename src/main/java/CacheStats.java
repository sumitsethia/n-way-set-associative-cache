import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class CacheStats {
    private long getCount = 0;
    private long putCount = 0;
    private long hitCount = 0;
    private long missCount = 0;
    private long evictionCount = 0;

    CacheStats() {
    }

    void add(CacheStats other) {
        this.getCount += other.getCount;
        this.putCount += other.putCount;
        this.hitCount += other.hitCount;
        this.missCount += other.missCount;
        this.evictionCount += other.evictionCount;
    }

    public long getRequestCount() {
        return getCount + putCount;
    }

    public double getHitRate() {
        if (hitCount == 0) {
            return 0;
        }
        return (getCount * 1.0) / hitCount;
    }

    public double getMissRate() {
        if (missCount == 0) {
            return 0;
        }
        return (getCount * 1.0) / missCount;
    }

}