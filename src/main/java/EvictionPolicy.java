import java.util.Iterator;

public interface EvictionPolicy {
    // Return the cache entry to evict based on the policy.
    // TODO since the cache entry is the original reference. User may modify it which shouldn't be allowed.
    <K, V> CacheEntry<K, V> policy(Iterator<CacheEntry<K, V>> iterator);
}
