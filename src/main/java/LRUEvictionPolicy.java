import java.util.Iterator;

public class LRUEvictionPolicy implements EvictionPolicy {
    @Override
    public <K, V> CacheEntry<K, V> policy(Iterator<CacheEntry<K, V>> iterator) {
        long leastRecentlyUsed = Long.MAX_VALUE;
        CacheEntry<K, V> result = null;

        while (iterator.hasNext()) {
            CacheEntry<K, V> cacheEntry = iterator.next();
            long lastAccess = cacheEntry.getLastAccessTime();
            if (lastAccess < leastRecentlyUsed) {
                result = cacheEntry;
                leastRecentlyUsed = lastAccess;
            }
        }
        return result;
    }
}
