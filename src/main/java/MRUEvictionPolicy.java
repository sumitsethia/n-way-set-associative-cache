import java.util.Iterator;

public class MRUEvictionPolicy implements EvictionPolicy {
    @Override
    public <K, V> CacheEntry<K, V> policy(Iterator<CacheEntry<K, V>> iterator) {
        long mostRecentlyUsed = -1;
        CacheEntry<K, V> result = null;

        while (iterator.hasNext()) {
            CacheEntry<K, V> cacheEntry = iterator.next();
            long lastAccess = cacheEntry.getLastAccessTime();
            if (lastAccess > mostRecentlyUsed) {
                result = cacheEntry;
                mostRecentlyUsed = lastAccess;
            }
        }
        return result;
    }
}
