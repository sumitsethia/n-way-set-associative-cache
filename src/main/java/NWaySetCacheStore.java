import lombok.NonNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class NWaySetCacheStore<K, V> implements Cache<K, V> {
    private List<AbstractCacheBlock<K, V>> cacheBlocks;
    private Integer cacheSize;

    NWaySetCacheStore(CacheBuilder<? super K, ? super V> cacheBuilder) {
        this.cacheSize = cacheBuilder.getCacheSize();
        cacheBlocks = new ArrayList<>();
        for (int i = 0; i < cacheBuilder.getCacheSize(); i++) {
            AbstractCacheBlock<K, V> abstractCacheBlock = new CacheBlockArrayListImp<K, V>(cacheBuilder.getBlockSize(), cacheBuilder.getEvictionPolicy(), cacheBuilder.getStatsCounterSupplier().get());
            cacheBlocks.add(abstractCacheBlock);
        }
    }

    public V get(@Nonnull K key) {
        Integer index = getIndex(key);
        AbstractCacheBlock<K, V> b = cacheBlocks.get(index);
        return b.get(key);
    }

    @Override
    public V getAndUpdate(K key, Updater<V> updater) {
        Integer index = getIndex(key);
        AbstractCacheBlock<K, V> b = cacheBlocks.get(index);
        return b.getAndUpdate(key, updater);
    }

    public void put(@NonNull K key, @NonNull V value) {
        Integer index = getIndex(key);
        AbstractCacheBlock<K, V> b = cacheBlocks.get(index);
        b.put(key, value);
    }

    @Override
    public boolean delete(@Nonnull K key) {
        Integer index = getIndex(key);
        AbstractCacheBlock<K, V> b = cacheBlocks.get(index);
        return b.delete(key);
    }

    @Override
    public CacheStats getCacheStats() {
        CacheStats cacheStats = new CacheStats();
        for (int i = 0; i < this.cacheSize; i++) {
            CacheStats blockStats = this.cacheBlocks.get(i).getCacheStats();
            cacheStats.add(blockStats);
        }
        return cacheStats;
    }

    @Override
    public Integer size() {
        int size = 0;
        for (int i = 0; i < this.cacheSize; i++) {
            size += this.cacheBlocks.get(i).size();
        }
        return size;
    }

    private Integer getIndex(K key) {
        Integer hash = (int) (Math.abs(key.hashCode() * 2654435761L) % this.cacheSize);
        return hash;
    }
}
