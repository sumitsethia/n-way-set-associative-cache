import java.util.concurrent.atomic.AtomicLong;

public interface StatsCounter {
    void recordHits(int count);

    void recordMisses(int count);

    void recordEviction(int count);

    void recordGet(int count);

    void recordPut(int count);

    CacheStats snapshot();
}

class NullStatsCounter implements StatsCounter {

    @Override
    public void recordHits(int count) {
    }

    @Override
    public void recordMisses(int count) {

    }

    @Override
    public void recordEviction(int count) {

    }

    @Override
    public void recordGet(int count) {

    }

    @Override
    public void recordPut(int count) {

    }

    @Override
    public CacheStats snapshot() {
        return new CacheStats();
    }
}

class StatsCounterImp implements StatsCounter {
    private AtomicLong hitCount;
    private AtomicLong missCount;
    private AtomicLong evictionCount;
    private AtomicLong getCount;
    private AtomicLong putCount;

    StatsCounterImp() {
        hitCount = new AtomicLong();
        missCount = new AtomicLong();
        evictionCount = new AtomicLong();
        getCount = new AtomicLong();
        putCount = new AtomicLong();
    }

    @Override
    public void recordHits(int count) {
        hitCount.addAndGet(count);
    }

    @Override
    public void recordMisses(int count) {
        missCount.addAndGet(count);
    }

    @Override
    public void recordEviction(int count) {
        evictionCount.addAndGet(count);
    }

    @Override
    public void recordGet(int count) {
        getCount.addAndGet(count);
    }

    @Override
    public void recordPut(int count) {
        putCount.addAndGet(count);
    }

    @Override
    public CacheStats snapshot() {
        return new CacheStats(getCount.get(), putCount.get(), hitCount.get(), missCount.get(), evictionCount.get());
    }
}