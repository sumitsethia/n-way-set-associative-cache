import java.time.Instant;

class TimeUtils {
    static long now() {
        return Instant.now().toEpochMilli();
    }
}
