public interface Updater<V> {
    // given a current value return new updated value.
    V update(V currentValue);

    boolean compare(V oldValue, V currentValue);
}
