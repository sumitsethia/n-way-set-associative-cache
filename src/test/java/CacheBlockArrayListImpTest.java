import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//TODO write more tests to check concurrency.

class CacheBlockArrayListImpTest {

    private CacheBlockArrayListImp<String, String> cacheBlockArrayList;

    @BeforeEach
    void setUp() throws Exception {
        cacheBlockArrayList = new CacheBlockArrayListImp<>(10, new LRUEvictionPolicy(), new StatsCounterImp());
    }

    @Test
    void evictThrows_Exception() {
        assertThrows(AssertionError.class, () -> cacheBlockArrayList.evict());
    }

    @Test
    void evictLRUPolicyTest() {
        cacheBlockArrayList.put("hello123", "world");
        cacheBlockArrayList.put("hello", "world");
        cacheBlockArrayList.evict();
        String value = cacheBlockArrayList.get("hello123");
        Assertions.assertNull(value);
    }

    @Test
    void evictMRUPolicyTest() throws Exception {
        cacheBlockArrayList = new CacheBlockArrayListImp<>(3, new MRUEvictionPolicy(), new StatsCounterImp());
        cacheBlockArrayList.put("hello123", "world");
        Thread.sleep(3);
        cacheBlockArrayList.put("hello", "world");
        cacheBlockArrayList.evict();
        String value = cacheBlockArrayList.get("hello123");
        Assertions.assertEquals("world", value);
        value = cacheBlockArrayList.get("hello");
        Assertions.assertNull(value);
    }

    @Test
    void testSimpleConcurrency() throws InterruptedException {
        Thread[] ar = new Thread[20];
        int z = 0;
        cacheBlockArrayList = new CacheBlockArrayListImp<>(10, new LRUEvictionPolicy(), new StatsCounterImp());
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            Thread t = new Thread("Put " + finalI) {
                public void run() {
                    try {
                        cacheBlockArrayList.put("1", Integer.toString(finalI));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            Thread t2 = new Thread("Get " + finalI) {
                public void run() {
                    try {
                        cacheBlockArrayList.getAndUpdate("1", new Updater<String>() {
                            @Override
                            public String update(String currentValue) {
                                return currentValue + " updated";
                            }

                            @Override
                            public boolean compare(String oldValue, String currentValue) {
                                return true;
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            t.start();
            t2.start();
            ar[z++] = t;
            ar[z++] = t2;
        }

        for (int i = 0; i < 20; i++) {
            ar[i].join();
        }

        Assertions.assertEquals(cacheBlockArrayList.size(), 1);
        CacheStats cacheStats = cacheBlockArrayList.getCacheStats();
        Assertions.assertEquals(cacheStats.getGetCount(), 10);
        Assertions.assertEquals(cacheStats.getPutCount(), 10);
    }
}