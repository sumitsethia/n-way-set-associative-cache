import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class nWaySetCacheStoreTest {

    Cache<String, String> cache;

    @BeforeEach
    void setUp() throws Exception {
        cache = CacheBuilder.newBuilder().cacheSize(2).blockSize(2).recordStats().build();
        cache.put("hello123", "world");
        cache.put("pankaj", "birat");
        cache.put("hello", "world");
    }

    @Test
    void get() {
        String value = cache.get("hello123");
        Assertions.assertEquals("world", value);
    }

    @Test
    void put() throws Exception {
        cache.put("hello", "world2");
        String value = cache.get("hello");
        Assertions.assertEquals("world2", value);
        cache.put("hello1", "world");
        value = cache.get("hello1");
        Assertions.assertEquals("world", value);
        cache.put("hello2", "world");
        value = cache.get("hello2");
        Assertions.assertEquals("world", value);
    }

    @Test
    void putNull_ThrowsNullPointerExceptionTest() {
        Assertions.assertThrows(NullPointerException.class, () -> cache.put("hey", null));
        Assertions.assertThrows(NullPointerException.class, () -> cache.put(null, "hey"));
    }

    @Test
    void delete() {
        boolean res = cache.delete("hello123");
        Assertions.assertTrue(res);
        String value = cache.get("hello123");
        Assertions.assertNull(value);
        res = cache.delete("dchdbchsdbc");
        Assertions.assertFalse(res);
    }

    @Test
    void getCacheStats() {
        CacheStats cacheStats = cache.getCacheStats();
        Assertions.assertEquals(cacheStats.getGetCount(), 0);
        Assertions.assertEquals(cacheStats.getPutCount(), 3);
        cache.get("random");
        cacheStats = cache.getCacheStats();
        Assertions.assertEquals(cacheStats.getHitCount(), 0);
        Assertions.assertEquals(cacheStats.getGetCount(), 1);
        Assertions.assertEquals(cacheStats.getMissCount(), 1);
        cache.get("hello123");
        cacheStats = cache.getCacheStats();
        Assertions.assertEquals(cacheStats.getHitCount(), 1);
        Assertions.assertEquals(cacheStats.getGetCount(), 2);
        Assertions.assertEquals(cacheStats.getMissCount(), 1);
    }

    @Test
    void size() {
        Integer size = cache.size();
        Assertions.assertEquals(3, size);
        cache.delete("hello123");
        size = cache.size();
        Assertions.assertEquals(2, size);
    }

    @Test
    void getAndUpdate() throws Exception {
        Cache<String, TestObj> cache = CacheBuilder.newBuilder().cacheSize(2).blockSize(2).recordStats().build();
        Updater<TestObj> updater = new Updater<TestObj>() {
            @Override
            public TestObj update(TestObj currentValue) {
                TestObj newValue = new TestObj();
                newValue.frequency = currentValue.frequency+1;
                newValue.value = currentValue.value;
                return newValue;
            }

            @Override
            public boolean compare(TestObj oldValue, TestObj currentValue) {
                return oldValue.value.equals(currentValue.value);
            }
        };
        cache.put("hello123", new TestObj("world", 1));
        cache.getAndUpdate("hello123", updater);
        cache.getAndUpdate("hello123", updater);
        TestObj v = cache.getAndUpdate("hello123", updater);
        Assertions.assertEquals(3, v.frequency);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    static class TestObj {
        String value;
        Integer frequency;
    }
}

